# domore-template-jsx

A template for domore using jsx

Install using
```bash
npm install
```

run webpack-dev-server using
```bash
npm start
```

or build a deployable static version using
```bash
npm run build
```

for reference see [DOMore](https://gitlab.com/smolamic/domore)
