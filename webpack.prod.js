const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: [
    './src/index.js',
  ],
  context: path.resolve(__dirname),
  mode: 'production',

  module: {
    rules: [{
      test: /\.(css|scss|sass)$/,
      use: [{
        loader: MiniCssExtractPlugin.loader,
      }, {
        loader: 'css-loader',
      }, {
        loader: 'sass-loader',
        options: {
          includePaths: ['src'],
        },
      }],
    }, {
      test: /\.m?jsx?$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          plugins: [
            ["@babel/plugin-transform-react-jsx", {
                "pragma": "DOMore.create",
                "pagmaFrag": "Array"
            }],
          ],
        },
      },
    }],
  },

  resolve: {
    extensions: ['.js', '.jsx'],
  },

  plugins: [
    new MiniCssExtractPlugin({ filename: 'app.css' }),
  ],

  output: {
    path: path.resolve(__dirname, 'dist/assets'),
    filename: 'bundle.js',
  },
};
