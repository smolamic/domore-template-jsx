const path = require('path');

module.exports = {
  entry: [
    './src/index.js',
  ],
  devtool: 'inline-source-map',
  context: path.resolve(__dirname),
  mode: 'development',

  module: {
    rules: [{
      test: /\.(css|scss|sass)$/,
      use: [{
        loader: 'style-loader',
        options: { sourceMap: true },
      }, {
        loader: 'css-loader',
        options: { sourceMap: true },
      }, {
        loader: 'sass-loader',
        options: {
          sourceMap: true,
          includePaths: ['src'],
        },
      }],
    }, {
      test: /\.m?jsx?$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          plugins: [
            ["@babel/plugin-transform-react-jsx", {
                "pragma": "DOMore.create",
                "pagmaFrag": "Array"
            }],
          ],
        },
      },
    }],
  },

  resolve: {
    extensions: ['.js', '.jsx'],
  },

  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    publicPath: '/assets/',
  },

  output: {
    path: path.resolve(__dirname, 'bundle'),
    filename: 'bundle.js',
  },
};
