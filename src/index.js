import DOMore from 'domore';
import App from './App';

const root = DOMore.get('body');

root.content = <App />;
