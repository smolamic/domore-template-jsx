import DOMore from 'domore';

const App = {
  base: 'section',
  dynamicContent: null,

  init() {
    this.content = (
      <div>
        <h1>This is a template</h1>
        <p>This is some placeholder-content</p>
        <p ref={(p) => { this.dynamicContent = p; }} />
      </div>
    );

    let index = 0;
    let variants = ['|', '/', '-', '\\'];
    setInterval(() => {
      this.text = variants[index];
      index = (index + 1) % variants.length;
    }, 500);
  },

  text: {
    get() {
      return this.dynamicContent.textContent;
    },
    set(text) {
      this.dynamicContent.textContent = text;
    },
  },
};

export { App as default };
